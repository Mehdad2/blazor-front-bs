﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace frontCarLocation.Data
{
    public class CarItems
    {
        public int Id { get; set; }
        public String brand { get; set; }
        public String color { get; set; }
        public Boolean IsAvalaible { get; set; }
    }
}
