﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace frontCarLocation.Data
{
    public class Steps
    {
        public String Name { get; set; }
        public int stepsNum { get; set; }
        public bool isViewed { get; set; }
    }
}
